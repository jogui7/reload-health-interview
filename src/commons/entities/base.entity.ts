import { v4 as uuid } from 'uuid';

export abstract class BaseEntity<T> {
  readonly id: string;

  constructor(data: Omit<T, 'id'>, id?: string) {
    this.id = id || uuid();
    Object.assign(this, data);
  }
}
