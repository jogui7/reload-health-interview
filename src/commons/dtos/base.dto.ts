export abstract class BaseDTO<T> {
  constructor(data?: Partial<T>) {
    Object.assign(this, data);
  }
}
