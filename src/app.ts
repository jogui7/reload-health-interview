import 'dotenv/config';

import cors from 'cors';
import restify from 'restify';
import helmet from 'helmet';
import { router } from './routes';
import { jsonBodyParser } from 'restify-plugins';

const app = restify.createServer();

app.use(helmet());
app.use(cors());
app.use(jsonBodyParser());

router.applyRoutes(app);

export default app;
