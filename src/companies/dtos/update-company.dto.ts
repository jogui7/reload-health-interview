import { Knex } from 'knex';
import { BaseDTO } from '../../commons/dtos';
import { Company } from '../entities';

export class UpdateCompanyDTO extends BaseDTO<UpdateCompanyDTO> {
  business_name?: string;
  suffix?: string;
  industry?: string;
  catch_phrase?: string;
  bs_company_statement?: string;
  logo?: string;
  type?: string;
  phone_number?: string;
  full_address?: string;
  coordinates?: { x: number; y: number };

  static toPartialEntity(dto: UpdateCompanyDTO, knex: Knex): Partial<Company> {
    return {
      business_name: dto.business_name,
      suffix: dto.suffix,
      industry: dto.industry,
      catch_phrase: dto.catch_phrase,
      bs_company_statement: dto.business_name,
      logo: dto.logo,
      type: dto.type,
      phone_number: dto.phone_number,
      full_address: dto.full_address,
      coordinates: dto.coordinates && knex.raw(`POINT(${dto.coordinates?.x}, ${dto.coordinates?.y})`)
    };
  }
}
