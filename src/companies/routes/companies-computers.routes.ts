import { Router } from 'restify-router';
import { Connection } from '../../database';
import { CompaniesComputersController } from '../controllers';
import { CompaniesComputersService } from '../services';

const connection = Connection.getInstance();
const companiesComputersService = new CompaniesComputersService(connection);
const companiesComputersController = new CompaniesComputersController(companiesComputersService);

export const companiesComputersRouter = new Router();

companiesComputersRouter.get('', (req, res, next) => companiesComputersController.list(req, res, next));
