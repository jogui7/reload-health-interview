import { createClient } from 'redis';
import { Router } from 'restify-router';
import { companiesComputersRouter, companiesContributorsRouter } from '.';
import { Connection } from '../../database';
import { redisConfig, RedisService } from '../../lib/redis';
import { CompaniesController } from '../controllers';
import { CompaniesService } from '../services';

const connection = Connection.getInstance();
const redisClient = createClient(redisConfig[process.env.NODE_ENV || 'development']);
const redisService = new RedisService(redisClient);
const companiesService = new CompaniesService(connection, redisService);
const companiesController = new CompaniesController(companiesService);

export const companiesRouter = new Router();

companiesRouter.get(
  '',
  (req, res, next) => redisService.cacheMiddleware(req, res, next, 'companies'),
  (req, res, next) => companiesController.list(req, res, next)
);

companiesRouter.get('/:id', (req, res, next) => companiesController.getById(req, res, next));

companiesRouter.get('/name/:name', (req, res, next) => companiesController.getByName(req, res, next));

companiesRouter.patch('/:id', (req, res, next) => companiesController.update(req, res, next));

companiesRouter.del('/:id', (req, res, next) => companiesController.deleteById(req, res, next));

companiesRouter.add('/:id/computers', companiesComputersRouter);

companiesRouter.add('/:id/contributors', companiesContributorsRouter);
