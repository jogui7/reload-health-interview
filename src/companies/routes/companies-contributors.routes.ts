import { Router } from 'restify-router';
import { Connection } from '../../database';
import { CompaniesContributorsController } from '../controllers';
import { CompaniesContributorsService } from '../services';

const connection = Connection.getInstance();
const companiesContributorsService = new CompaniesContributorsService(connection);
const companiesContributorsController = new CompaniesContributorsController(companiesContributorsService);

export const companiesContributorsRouter = new Router();

companiesContributorsRouter.get('', (req, res, next) => companiesContributorsController.list(req, res, next));
