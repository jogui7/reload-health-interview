import { Next, Request, Response } from 'restify';
import { CompaniesService } from '../services';

export class CompaniesController {
  constructor(private readonly companiesService: CompaniesService) {}

  async list(_request: Request, response: Response, _next: Next) {
    try {
      const companies = await this.companiesService.list();
      return response.json(companies);
    } catch (err) {
      response.json(err || 'Unexpected Error');
    }
  }

  async getById(request: Request, response: Response, _next: Next) {
    try {
      const { params } = request;

      const company = await this.companiesService.getById(params.id);

      return response.json(company);
    } catch (err) {
      response.json(err || 'Unexpected Error');
    }
  }

  async getByName(request: Request, response: Response, _next: Next) {
    try {
      const { params } = request;

      const company = await this.companiesService.getByName(params.name);

      return response.json(company);
    } catch (err) {
      response.json(err || 'Unexpected Error');
    }
  }

  async update(request: Request, response: Response, _next: Next) {
    try {
      const { params, body } = request;

      await this.companiesService.update(params.id, body);

      return response.send(200);
    } catch (err) {
      response.json(err || 'Unexpected Error');
    }
  }

  async deleteById(request: Request, response: Response, _next: Next) {
    try {
      const { params } = request;

      await this.companiesService.deleteById(params.id);

      return response.send(200);
    } catch (err) {
      response.json(err || 'Unexpected Error');
    }
  }
}
