import { Next, Request, Response } from 'restify';
import { CompaniesContributorsService } from '../services';

export class CompaniesContributorsController {
  constructor(private readonly companiesContributorsService: CompaniesContributorsService) {}

  async list(request: Request, response: Response, _next: Next) {
    try {
      const { params } = request;

      const computers = await this.companiesContributorsService.list(params.id);

      return response.json(computers);
    } catch (err) {
      response.json(err || 'Unexpected Error');
    }
  }
}
