import { Next, Request, Response } from 'restify';
import { CompaniesComputersService } from '../services';

export class CompaniesComputersController {
  constructor(private readonly companiesComputersService: CompaniesComputersService) {}

  async list(request: Request, response: Response, _next: Next) {
    try {
      const { params } = request;

      const computers = await this.companiesComputersService.list(params.id);

      return response.json(computers);
    } catch (err) {
      response.json(err || 'Unexpected Error');
    }
  }
}
