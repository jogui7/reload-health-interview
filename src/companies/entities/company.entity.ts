import { BaseEntity } from '../../commons/entities';

export class Company extends BaseEntity<Company> {
  business_name: string;
  suffix: string;
  industry: string;
  catch_phrase: string;
  bs_company_statement: string;
  logo: string;
  type: string;
  phone_number: string;
  full_address: string;
  coordinates: any;
}
