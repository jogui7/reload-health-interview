import { Contributor } from '../../contributors/entities';
import { Connection } from '../../database';

export class CompaniesContributorsService {
  constructor(private readonly connection: Connection) {}

  async list(companyId: string) {
    const queryBuilder = this.connection.knex;

    return queryBuilder.select('*').from<Contributor>('contributors').where('company_id', companyId);
  }
}
