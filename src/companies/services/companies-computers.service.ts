import { Computer } from '../../computers/entities';
import { Connection } from '../../database';

export class CompaniesComputersService {
  constructor(private readonly connection: Connection) {}

  async list(companyId: string) {
    const queryBuilder = this.connection.knex;

    return queryBuilder.select('*').from<Computer>('computers').where('company_id', companyId);
  }
}
