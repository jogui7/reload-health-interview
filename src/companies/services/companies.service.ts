import { BadRequestError, NotFoundError } from 'restify-errors';
import { Connection } from '../../database';
import { RedisService } from '../../lib/redis';
import { UpdateCompanyDTO } from '../dtos';
import { Company } from '../entities';

export class CompaniesService {
  constructor(private readonly connection: Connection, private readonly redisService: RedisService) {}

  async list() {
    const queryBuilder = this.connection.knex;

    const companies = await queryBuilder.select('*').from<Company>('companies');

    await this.redisService.save('companies', companies);

    return companies;
  }

  async getById(id: string) {
    const queryBuilder = this.connection.knex;

    const company = await queryBuilder.select('*').from<Company>('companies').where('id', id).first();

    if (!company) {
      throw new NotFoundError('Company does not exists');
    }

    return company;
  }

  async getByName(name: string) {
    const queryBuilder = this.connection.knex;

    const company = await queryBuilder.select('*').from<Company>('companies').where('business_name', name).first();

    if (!company) {
      throw new NotFoundError('Company does not exists');
    }
    return company;
  }

  async update(id: string, body: UpdateCompanyDTO) {
    const queryBuilder = this.connection.knex;

    const updated = await queryBuilder
      .from<Company>('companies')
      .where('id', id)
      .update(UpdateCompanyDTO.toPartialEntity(body, queryBuilder));

    if (!updated) {
      throw new BadRequestError('Error when updating company');
    }
  }

  async deleteById(id: string) {
    const queryBuilder = this.connection.knex;

    const deleted = await queryBuilder.del().from<Company>('companies').where('id', id);

    if (!deleted) {
      throw new BadRequestError('Error when deleting company');
    }
  }
}
