import { createClient } from 'redis';
import { Router } from 'restify-router';
import { Connection } from '../../database';
import { redisConfig, RedisService } from '../../lib/redis';
import { ComputersController } from '../controllers';
import { ComputersService } from '../services';

const connection = Connection.getInstance();
const redisClient = createClient(redisConfig[process.env.NODE_ENV || 'development']);
const redisService = new RedisService(redisClient);
const computersService = new ComputersService(connection, redisService);
const computersController = new ComputersController(computersService);

export const computersRouter = new Router();

computersRouter.get(
  '',
  (req, res, next) => redisService.cacheMiddleware(req, res, next, 'computers'),
  (req, res, next) => computersController.list(req, res, next)
);

computersRouter.post('', (req, res, next) => computersController.create(req, res, next));

computersRouter.del('/:id', (req, res, next) => computersController.deleteById(req, res, next));
