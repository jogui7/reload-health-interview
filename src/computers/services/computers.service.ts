import { BadRequestError } from 'restify-errors';
import { Connection } from '../../database';
import { RedisService } from '../../lib/redis';
import { CreateComputerDTO } from '../dtos';
import { Computer } from '../entities';

export class ComputersService {
  constructor(private readonly connection: Connection, private readonly redisService: RedisService) {}

  async list() {
    const queryBuilder = this.connection.knex;

    const computers = await queryBuilder.select('*').from<Computer>('computers');

    await this.redisService.save('computers', computers);

    return computers;
  }

  async create(body: CreateComputerDTO) {
    const queryBuilder = this.connection.knex;

    const saved = await queryBuilder.insert(CreateComputerDTO.toEntity(body)).into<Computer>('computers');

    if (!saved) {
      throw new BadRequestError('Error when creating computer');
    }
  }

  async deleteById(id: string) {
    const queryBuilder = this.connection.knex;

    const deleted = await queryBuilder.del().from<Computer>('computers').where('id', id);

    if (!deleted) {
      throw new BadRequestError('Error when deleting computer');
    }
  }
}
