import { Next, Request, Response } from 'restify';
import { ComputersService } from '../services';

export class ComputersController {
  constructor(private readonly computersService: ComputersService) {}

  async list(_request: Request, response: Response, _next: Next) {
    try {
      const computers = await this.computersService.list();
      return response.json(computers);
    } catch (err) {
      response.json(err || 'Unexpected Error');
    }
  }

  async create(request: Request, response: Response, _next: Next) {
    try {
      const { body } = request;

      await this.computersService.create(body);

      return response.send(201);
    } catch (err) {
      response.json(err || 'Unexpected Error');
    }
  }

  async deleteById(request: Request, response: Response, _next: Next) {
    try {
      const { params } = request;

      await this.computersService.deleteById(params.id);

      return response.send(200);
    } catch (err) {
      response.json(err || 'Unexpected Error');
    }
  }
}
