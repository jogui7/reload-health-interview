import { BaseEntity } from '../../commons/entities';

export class Computer extends BaseEntity<Computer> {
  platform: string;
  type: string;
  os: string;
  ip: string;
  company_id: string;
}
