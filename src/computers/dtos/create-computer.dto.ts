import { BaseDTO } from '../../commons/dtos';
import { Computer } from '../entities';

export class CreateComputerDTO extends BaseDTO<CreateComputerDTO> {
  platform: string;
  type: string;
  os: string;
  ip: string;
  company_id: string;

  static toEntity(dto: CreateComputerDTO): Computer {
    return new Computer({
      platform: dto.platform,
      type: dto.type,
      os: dto.os,
      ip: dto.ip,
      company_id: dto.company_id
    });
  }
}
