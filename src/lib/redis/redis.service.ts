import { RedisClientType, RedisScripts } from 'redis';
import { Next, Request, Response } from 'restify';

export class RedisService {
  constructor(private redisClient: RedisClientType<Record<string, any>, RedisScripts>) {}

  async save<T>(key: string, data: T) {
    await this.redisClient.connect();

    await this.redisClient.set(key, JSON.stringify(data), {
      EX: 3600,
      NX: true
    });

    await this.redisClient.quit();
  }

  private async get<T>(key: string): Promise<T | null> {
    await this.redisClient.connect();

    const data = await this.redisClient.get(key);

    await this.redisClient.quit();

    if (data) {
      return JSON.parse(data);
    }

    return null;
  }

  async cacheMiddleware(request: Request, response: Response, next: Next, key: string) {
    const { url, method } = request;

    const data = await this.get(key);

    if (data) {
      console.log(`[RedisService] ${method} on ${url} cached`);
      response.send(data);
    } else {
      console.log(`[RedisService] ${method} on ${url} fetched`);
      next();
    }
  }
}
