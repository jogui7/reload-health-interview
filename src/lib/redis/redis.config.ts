import 'dotenv/config';
import { RedisClientOptions, RedisScripts } from 'redis';

export const redisConfig: { [key: string]: Omit<RedisClientOptions<never, RedisScripts>, 'modules'> } = {
  development: {
    socket: {
      host: 'localhost',
      port: 6379
    }
  },

  production: {
    socket: {
      host: 'redis',
      port: 6379
    }
  }
};
