import { Knex } from 'knex';
import { dataset } from '../dataset';
import {
  CreateCompanyFromDatasetDTO,
  CreateComputerFromDatasetDTO,
  CreateContributorFromDatasetDTO
} from '../dataset/dtos';

export async function seed(knex: Knex): Promise<void> {
  console.log('[database] Seeding database...');

  await knex('contributors').del();
  await knex('computers').del();
  await knex('companies').del();

  for (const company of dataset) {
    const { id, contributors, computers } = await insertCompany(knex, company);

    await insertContributors(knex, contributors, id);
    await insertComputers(knex, computers, id);
  }

  console.log('[database] Finished seeding database...');
}

const insertCompany = async (knex: Knex, company: CreateCompanyFromDatasetDTO) => {
  const entityFactory = new EntityFactory();

  const entity = entityFactory.create(company, 'COMPANY', knex, '');

  await knex('companies').insert(entity);

  return {
    id: entity.id,
    contributors: company.contributors,
    computers: company.desktops
  };
};

const insertContributors = async (knex: Knex, contributors: CreateContributorFromDatasetDTO[], companyId: string) => {
  const entityFactory = new EntityFactory();

  const entities = contributors.map(contributor => entityFactory.create(contributor, 'CONTRIBUTOR', knex, companyId));

  await knex('contributors').insert(entities);
};

const insertComputers = async (knex: Knex, computers: CreateComputerFromDatasetDTO[], companyId: string) => {
  const entityFactory = new EntityFactory();

  const entities = computers.map(computer => entityFactory.create(computer, 'COMPUTER', knex, companyId));

  await knex('computers').insert(entities);
};

class EntityFactory {
  public create = (
    data: CreateCompanyFromDatasetDTO | CreateContributorFromDatasetDTO | CreateComputerFromDatasetDTO,
    type: 'COMPANY' | 'CONTRIBUTOR' | 'COMPUTER',
    knex: Knex,
    companyId: string
  ) => {
    switch (type) {
      case 'COMPANY':
        return CreateCompanyFromDatasetDTO.toEntity(data as CreateCompanyFromDatasetDTO, knex);
        break;
      case 'CONTRIBUTOR':
        return CreateContributorFromDatasetDTO.toEntity(data as CreateContributorFromDatasetDTO, companyId);
        break;
      case 'COMPUTER':
        return CreateComputerFromDatasetDTO.toEntity(data as CreateComputerFromDatasetDTO, companyId);
        break;
    }
  };
}
