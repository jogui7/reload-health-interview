import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('companies', table => {
    table.string('id', 36).primary();
    table.text('business_name');
    table.text('suffix');
    table.text('industry');
    table.text('catch_phrase');
    table.text('bs_company_statement');
    table.text('logo');
    table.text('type');
    table.text('phone_number');
    table.text('full_address');
    table.specificType('coordinates', 'POINT');
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('companies');
}
