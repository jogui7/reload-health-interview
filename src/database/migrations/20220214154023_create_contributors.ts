import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('contributors', table => {
    table.string('id', 36).primary();
    table.text('firstName');
    table.text('lastName');
    table.text('title');
    table.text('jobTitle');
    table.smallint('age');
    table.string('company_id', 36).notNullable();
    table.foreign('company_id').references('companies.id').onDelete('CASCADE');
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('contributors');
}
