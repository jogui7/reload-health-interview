import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('computers', table => {
    table.string('id', 36).primary();
    table.text('platform');
    table.text('type');
    table.text('os');
    table.text('ip');
    table.string('company_id', 36).notNullable();
    table.foreign('company_id').references('companies.id').onDelete('CASCADE');
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('computers');
}
