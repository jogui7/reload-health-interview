import 'dotenv/config';
import type { Knex } from 'knex';
import { Databases } from './enums';

const knexConfig: { [key: string]: Knex.Config } = {
  development: {
    client: Databases.MySQL,
    connection: {
      host: process.env.DB_HOST || '127.0.0.1',
      user: process.env.DB_USER || 'root',
      password: process.env.DB_PASSWORD || 'reload-test',
      database: process.env.DB_DATABASE || 'reload_test'
    },
    migrations: {
      tableName: 'migrations',
      directory: `${__dirname}/migrations`,
      extension: 'ts'
    },
    seeds: {
      directory: `${__dirname}/seeds`,
      extension: 'ts'
    }
  },

  test: {
    client: Databases.MySQL,
    connection: {
      host: process.env.DB_HOST || '127.0.0.1',
      user: process.env.DB_USER || 'root',
      password: process.env.DB_PASSWORD || 'reload-test',
      database: process.env.DB_DATABASE || 'reload_test'
    },
    migrations: {
      tableName: 'migrations',
      directory: `${__dirname}/migrations`,
      extension: 'ts'
    },
    seeds: {
      directory: `${__dirname}/seeds`,
      extension: 'ts'
    }
  },

  production: {
    client: Databases.MySQL,
    connection: {
      host: process.env.DB_HOST_PROD || 'mysql',
      user: process.env.DB_USER || 'root',
      password: process.env.DB_PASSWORD || 'reload-test',
      database: process.env.DB_DATABASE || 'reload_test'
    },
    migrations: {
      tableName: 'migrations',
      directory: `${__dirname}/migrations`,
      extension: 'ts'
    },
    seeds: {
      directory: `${__dirname}/seeds`,
      extension: 'ts'
    }
  }
};

export default knexConfig;
