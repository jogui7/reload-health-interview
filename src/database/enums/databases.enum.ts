export enum Databases {
  PostgreSQL = 'pg',
  CockroachDB = 'pg',
  MSSQL = 'tedious',
  MySQL = 'mysql',
  MariaDB = 'mysql',
  SQLite3 = 'sqlite3',
  Better_SQLite3 = 'better-sqlite3',
  Oracle = 'oracledb',
  Amazon_Redshift = 'pg'
}
