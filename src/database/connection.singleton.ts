import { knex as queryBuilder } from 'knex';
import knexConfig from './knexfile';

export class Connection {
  private static instance: Connection;

  private _knex;

  private constructor() {
    this._knex = queryBuilder(knexConfig[process.env.NODE_ENV || 'development']);
  }

  public static getInstance(): Connection {
    if (!Connection.instance) {
      Connection.instance = new Connection();
    }

    return Connection.instance;
  }

  public get knex() {
    return this._knex;
  }
}
