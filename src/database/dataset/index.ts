import datasetFile from './dataset.json';
import { CreateCompanyFromDatasetDTO } from './dtos';

export const dataset: CreateCompanyFromDatasetDTO[] = JSON.parse(JSON.stringify(datasetFile));
