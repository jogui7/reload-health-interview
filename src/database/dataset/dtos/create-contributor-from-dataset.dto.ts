import { BaseDTO } from '../../../commons/dtos';
import { Contributor } from '../../../contributors/entities';

export class CreateContributorFromDatasetDTO extends BaseDTO<CreateContributorFromDatasetDTO> {
  firstName: string;
  lastName: string;
  title: string;
  jobTitle: string;
  age: number;

  static toEntity(dto: CreateContributorFromDatasetDTO, companyId: string): Contributor {
    return new Contributor({
      firstName: dto.firstName,
      lastName: dto.lastName,
      title: dto.title,
      jobTitle: dto.jobTitle,
      age: dto.age,
      company_id: companyId
    });
  }
}
