import { BaseDTO } from '../../../commons/dtos';
import { Computer } from '../../../computers/entities';

export class CreateComputerFromDatasetDTO extends BaseDTO<CreateComputerFromDatasetDTO> {
  id: number;
  platform: string;
  type: string;
  os: string;
  ip: string;

  static toEntity(dto: CreateComputerFromDatasetDTO, companyId: string): Computer {
    return new Computer({
      platform: dto.platform,
      type: dto.type,
      os: dto.os,
      ip: dto.ip,
      company_id: companyId
    });
  }
}
