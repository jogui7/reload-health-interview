import { Knex } from 'knex';
import { CreateComputerFromDatasetDTO, CreateContributorFromDatasetDTO } from '.';
import { BaseDTO } from '../../../commons/dtos';
import { Company } from '../../../companies/entities';

export class CreateCompanyFromDatasetDTO extends BaseDTO<CreateCompanyFromDatasetDTO> {
  id: number;
  business_name: string;
  suffix: string;
  industry: string;
  catch_phrase: string;
  bs_company_statement: string;
  logo: string;
  type: string;
  phone_number: string;
  full_address: string;
  latitude: number;
  longitude: number;
  contributors: CreateContributorFromDatasetDTO[];
  desktops: CreateComputerFromDatasetDTO[];

  static toEntity(dto: CreateCompanyFromDatasetDTO, knex: Knex): Company {
    return new Company({
      business_name: dto.business_name,
      suffix: dto.suffix,
      industry: dto.industry,
      catch_phrase: dto.catch_phrase,
      bs_company_statement: dto.bs_company_statement,
      logo: dto.logo,
      type: dto.type,
      phone_number: dto.phone_number,
      full_address: dto.full_address,
      coordinates: knex.raw(`POINT(${dto.latitude}, ${dto.longitude})`)
    });
  }
}
