import { Connection } from '..';

describe('Connection', () => {
  it('should be a singleton', () => {
    const connection1 = Connection.getInstance();
    const connection2 = Connection.getInstance();

    expect(connection1).toEqual(connection2);
  });
});
