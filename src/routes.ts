import { Router } from 'restify-router';
import { companiesRouter } from './companies/routes';
import { computersRouter } from './computers/routes';
import { contributorsRouter } from './contributors/routes';

const router = new Router();

router.add('/companies', companiesRouter);
router.add('/computers', computersRouter);
router.add('/contributors', contributorsRouter);

export { router };
