import { BadRequestError } from 'restify-errors';
import { Connection } from '../../database';
import { RedisService } from '../../lib/redis';
import { CreateContributorDTO, UpdateContributorDTO } from '../dtos';
import { Contributor } from '../entities';

export class ContributorsService {
  constructor(private readonly connection: Connection, private readonly redisService: RedisService) {}

  async list() {
    const queryBuilder = this.connection.knex;

    const contributors = await queryBuilder.select('*').from<Contributor>('contributors');

    await this.redisService.save('contributors', contributors);

    return contributors;
  }

  async create(body: CreateContributorDTO) {
    const queryBuilder = this.connection.knex;

    const saved = await queryBuilder.insert(CreateContributorDTO.toEntity(body)).into<Contributor>('contributors');

    if (!saved) {
      throw new BadRequestError('Error when creating contributor');
    }
  }

  async update(id: string, body: UpdateContributorDTO) {
    const queryBuilder = this.connection.knex;

    const updated = await queryBuilder
      .from<Contributor>('contributors')
      .where('id', id)
      .update(UpdateContributorDTO.toPartialEntity(body));

    if (!updated) {
      throw new BadRequestError('Error when updating contributor');
    }
  }

  async deleteById(id: string) {
    const queryBuilder = this.connection.knex;

    const deleted = await queryBuilder.del().from<Contributor>('contributors').where('id', id);

    if (!deleted) {
      throw new BadRequestError('Error when deleting contributor');
    }
  }
}
