import { Next, Request, Response } from 'restify';
import { ContributorsService } from '../services';

export class ContributorsController {
  constructor(private readonly contributorsService: ContributorsService) {}

  async list(_request: Request, response: Response, _next: Next) {
    try {
      const contributors = await this.contributorsService.list();
      return response.json(contributors);
    } catch (err) {
      response.json(err || 'Unexpected Error');
    }
  }

  async create(request: Request, response: Response, _next: Next) {
    try {
      const { body } = request;

      await this.contributorsService.create(body);

      return response.send(201);
    } catch (err) {
      response.json(err || 'Unexpected Error');
    }
  }

  async update(request: Request, response: Response, _next: Next) {
    try {
      const { params, body } = request;

      await this.contributorsService.update(params.id, body);

      return response.send(200);
    } catch (err) {
      response.json(err || 'Unexpected Error');
    }
  }

  async deleteById(request: Request, response: Response, _next: Next) {
    try {
      const { params } = request;

      await this.contributorsService.deleteById(params.id);

      return response.send(200);
    } catch (err) {
      response.json(err || 'Unexpected Error');
    }
  }
}
