import app from '../../app';
import request from 'supertest';
import { Company } from '../../companies/entities';

describe('ContributorsController', () => {
  let company: Company;

  beforeAll(async () => {
    const response = await request(app).get('/companies');

    company = response.body[0];
  });

  it('should be able to create a new contributor', async () => {
    const response = await request(app).post('/contributors').send({
      firstName: 'Joao',
      lastName: 'Guis',
      title: 'Software Developer',
      jobTitle: 'Jr. Software Developer',
      age: 20,
      company_id: company.id
    });

    expect(response.status).toEqual(201);
  });

  it('should be able to delete a contributor', async () => {
    const fetch = await request(app).get(`/companies/${company.id}/contributors`);

    const contributor = fetch.body[0];

    const response = await request(app).delete(`/contributors/${contributor.id}`);

    expect(response.status).toEqual(200);
  });
});
