import { BaseEntity } from '../../commons/entities';

export class Contributor extends BaseEntity<Contributor> {
  firstName: string;
  lastName: string;
  title: string;
  jobTitle: string;
  age: number;
  company_id: string;
}
