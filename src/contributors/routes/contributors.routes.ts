import { createClient } from 'redis';
import { Router } from 'restify-router';
import { Connection } from '../../database';
import { redisConfig, RedisService } from '../../lib/redis';
import { ContributorsController } from '../controllers';
import { ContributorsService } from '../services';

const connection = Connection.getInstance();
const redisClient = createClient(redisConfig[process.env.NODE_ENV || 'development']);
const redisService = new RedisService(redisClient);
const contributorsService = new ContributorsService(connection, redisService);
const contributorsController = new ContributorsController(contributorsService);

export const contributorsRouter = new Router();

contributorsRouter.get(
  '',
  (req, res, next) => redisService.cacheMiddleware(req, res, next, 'contributors'),
  (req, res, next) => contributorsController.list(req, res, next)
);

contributorsRouter.post('', (req, res, next) => contributorsController.create(req, res, next));

contributorsRouter.patch('/:id', (req, res, next) => contributorsController.update(req, res, next));

contributorsRouter.del('/:id', (req, res, next) => contributorsController.deleteById(req, res, next));
