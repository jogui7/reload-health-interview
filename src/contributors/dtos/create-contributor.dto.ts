import { BaseDTO } from '../../commons/dtos';
import { Contributor } from '../entities';

export class CreateContributorDTO extends BaseDTO<CreateContributorDTO> {
  firstName: string;
  lastName: string;
  title: string;
  jobTitle: string;
  age: number;
  company_id: string;

  static toEntity(dto: CreateContributorDTO): Contributor {
    return new Contributor({
      firstName: dto.firstName,
      lastName: dto.lastName,
      title: dto.title,
      jobTitle: dto.jobTitle,
      age: dto.age,
      company_id: dto.company_id
    });
  }
}
