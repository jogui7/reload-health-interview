import { BaseDTO } from '../../commons/dtos';
import { Contributor } from '../entities';

export class UpdateContributorDTO extends BaseDTO<UpdateContributorDTO> {
  firstName?: string;
  lastName?: string;
  title?: string;
  jobTitle?: string;
  age?: number;

  static toPartialEntity(dto: UpdateContributorDTO): Partial<Contributor> {
    return {
      firstName: dto.firstName,
      lastName: dto.lastName,
      title: dto.title,
      jobTitle: dto.jobTitle,
      age: dto.age
    };
  }
}
