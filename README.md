<h3  align="center">



This API was developed during the tech interview for **[Reload Health][reaload_health_site]** using the following techs:  ***TypeScript, Node, Restify e Redis***.



</h3>



## **:rocket: OBJECTIVE**


The project has the objective to seed a [MySQL][Mysql] database using [Knex][Knex] and make requests through Restful API.



## **:computer: Techs**




#### **API** ([NodeJS][node] + [TypeScript][typescript])



- **[Restify][Restify]**

- **[Knex][Knex]**

- **[MySQL][MySQL]**

- **[Redis][Redis]**

- **[ts-node-dev][tsnodedev]**

- **[Docker][docker]**

- **[Jest][jest]**



#### **Utils**


- Editor: **[Visual Studio Code][vscode]**

- API Testing: **[Insomnia][insomnia]**


## **:books:  Methodology**



The development of this project, was based on some SOLID principles, were applied Factory and Singleton Patterns and Unit/Integation tests with [Jest][Jest].



## **:wine_glass: HOW TO USE**

*make sure you have node, docker and git installed*


### Cloning repository

```sh
$ git clone https://gitlab.com/jogui7/reload-health-interview
```

## Development mode:

### Install dependencies

```sh
$ npm install
```
Or if you use [Yarn][yarn]

```sh
$ yarn
```

### Initialize Redis and MySQL

```sh
$ cd ./docker-reload
$ docker compose up -d
```

### Run migrations and seed database
```sh
$ yarn migration:up
$ yarn seed:run
```

### Start Server
```sh
$ yarn dev
```
### Testing
```sh
$ yarn test
```

## Production mode:

### Build API docker image
```sh
$ docker build -f DockerFile -t reload-health-interview .
```

### Running API, Redis and MySQL
```sh
$ cd ./docker-reload/production
$ docker compose up -d
```

> See **scripts {}** in <kbd>[package.json](./package.json)</kbd> to see available scripts

> Import <kbd>[Insomnia collection](./reload_test_insomnia.json)</kbd> to use endpoints


## **:books: DOCS**

- [TypeScript](https://www.typescriptlang.org/docs/home.html)

- [Knex][Knex]

- [Docker](https://docs.docker.com)

- [Redis](https://redis.io/documentation)

- [Restify](http://restify.com/docs/home/)

- [Node](https://nodejs.org/en/)

- [Jest](https://jestjs.io/docs/getting-started)





## **:page_with_curl: LICENSE**



 This project is licensed under the terms of **MIT LICENSE**. For more information, read [LICENSE](./LICENSE) file.



<h3  align="center">

Made by <a  href="https://www.linkedin.com/in/joao-guis/?locale=en_US/">João Guilherme Bini Guis</a>

<br><br>


</a>

</h3>



<!-- Website Links -->



[reaload_health_site]:  https://reload.co



<!-- Badges -->



[BADGE_CLOSED_ISSUES]:  https://img.shields.io/github/issues-closed/x0n4d0/ecoleta?color=red



[BADGE_OPEN_ISSUES]:  https://img.shields.io/github/issues/x0n4d0/ecoleta?color=green



[BADGE_LICENSE]:  https://img.shields.io/github/license/x0n4d0/ecoleta



[BADGE_NODE_VERSION]:  https://img.shields.io/badge/node-12.17.0-green



[BADGE_NPM_VERSION]:  https://img.shields.io/badge/npm-6.14.4-red



[BADGE_WEB_REACT]:  https://img.shields.io/badge/web-react-blue



[BADGE_MOBILE_REACT_NATIVE]:  https://img.shields.io/badge/mobile-react%20native-blueviolet



[BADGE_SERVER_NODEJS]:  https://img.shields.io/badge/server-nodejs-important



[BADGE_STARS]:  https://img.shields.io/github/stars/x0n4d0/ecoleta?style=social



[BADGE_FORKS]:  https://img.shields.io/github/forks/x0n4d0/ecoleta?style=social



[BADGE_TYPESCRIPT]:  https://badges.frapsoft.com/typescript/code/typescript.png?v=101



[BADGE_OPEN_SOURCE]:  https://badges.frapsoft.com/os/v1/open-source.png?v=103



<!-- Techs -->


[typescript]:  https://www.typescriptlang.org/



[node]:  https://nodejs.org/en/



[vscode]:  https://code.visualstudio.com/



[restify]:  https://restify.com/



[knex]:  https://knexjs.org



[redis]: https://redis.io



[Docker]:  https://www.docker.com



[mysql]:  https:///www.mysql.com



[tsnodedev]:  https://www.npmjs.com/package/ts-node-dev



[insomnia]:  https://insomnia.rest/



[jest]:  https://jestjs.io



[yarn]:  https://classic.yarnpkg.com/en/docs/install/#debian-stable
